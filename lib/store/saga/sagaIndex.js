import { all } from "redux-saga/effects";
import SagaPage from "../saga/sagaPage";

export default function* rootsaga() {
  yield all([SagaPage()]);
}
