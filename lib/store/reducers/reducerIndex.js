import { combineReducers } from "redux";
import SendReducer from "../../../reducer/reducer";

const Reducer = combineReducers({
  SendReducer
});

export default Reducer;
