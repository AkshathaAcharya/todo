const STORE_TASK = "STORE_TASK";

export const storeIt = data => {
  return {
    type: STORE_TASK,
    payload: data
  };
};
