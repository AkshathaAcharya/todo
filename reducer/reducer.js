import getReducerFromObject from "../utils/reducerUtils";
var storeTaskArray = [];
const initialState = {
  storeArray: []
};

const SendReducer = getReducerFromObject([], {
  STORE_TASK: (state, action) => {
    storeTaskArray.push(...action.payload);

    console.log(storeTaskArray);
    return {
      storeArray: [...storeTaskArray]
    };
  }
});

export default SendReducer;
