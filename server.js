const express = require("express");
const next = require("next");
const bodyParser = require("body-parser");
const port = parseInt(process.env.PORT, 10) || 3050;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const cors = require("cors");

app
  .prepare()
  .then(() => {
    const server = express();

    server.use(bodyParser.urlencoded({ extended: true }));
    server.use(bodyParser.json());

    server.get("*", (req, res) => {
      handle(req, res);
    });

    server.use(cors());

    server.listen(port, () => console.log(`server stated on port ${port}`));
  })
  .catch(ex => {
    console.log(ex.stack);
    process.exit(1);
  });
