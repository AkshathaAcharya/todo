import React from "react";
import { Draggable, Droppable } from "react-drag-and-drop";
import { storeIt } from "../reducer/action";
import { connect } from "react-redux";
import "antd/dist/antd.css";
import "../styles/layout.css";
import {
  Layout,
  Menu,
  Input,
  Modal,
  Button,
  Table,
  Select,
  DatePicker,
  Tag
} from "antd";

import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  HomeTwoTone,
  BellOutlined,
  DownloadOutlined,
  FileDoneOutlined,
  UserAddOutlined,
  SearchOutlined,
  QuestionOutlined,
  FileAddOutlined,
  TableOutlined,
  UserOutlined
} from "@ant-design/icons";

const { Sider, Content } = Layout;
const { Option } = Select;
var ProjectArray = [];
var TaskArray = [];
var MainProjectName = "";

class Scale extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      collapsed: false,
      visible: false,
      show: false,
      projectName: null,
      projectDescription: null,
      Project: [],
      pageDetails: null,
      arrowDown: false,
      dateDisplay: null,
      things: null,
      user: null,
      status: null,
      dateUser: null,
      priority: null,
      mainName: null
    };
  }
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };
  showModal = () => {
    this.setState({
      visible: true,
      projectName: null,
      projectDescription: null
    });
  };
  handleOk = e => {
    this.setState({
      visible: false,
      show: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
      show: false
    });
  };
  addName = e => {
    this.setState({
      projectName: e === undefined ? null : e.target.value
    });
  };
  addDescription = e => {
    this.setState({
      projectDescription: e === undefined ? null : e.target.value
    });
  };
  addProject = () => {
    this.state.projectName !== null &&
      ProjectArray.push(
        ...[
          {
            name: this.state.projectName,
            description: this.state.projectDescription
          }
        ]
      );
    this.setState({ projectName: null, projectDescription: null });
    document.getElementById("myInput").value = null;
    document.getElementById("fieldInput").value = null;
  };
  pageData = e => {
    this.setState({ pageData: ProjectArray[e.key] });
  };
  toggleTable = () => {
    this.setState({ arrowDown: !this.state.arrowDown });
  };
  handleAdd = () => {
    {
      this.state.pageData !== undefined
        ? this.setState({ show: !this.state.show })
        : alert("Add Main Project");
    }
  };

  onBlur = () => {
    console.log("blur");
  };

  onFocus = () => {
    console.log("focus");
  };

  onSearch = val => {
    console.log("search:", val);
  };

  addThings = e => {
    this.setState({ things: e === undefined ? null : e.target.value });
  };
  addUser = e => {
    this.setState({ user: e === undefined ? null : e.target.value });
  };
  showStatus = e => {
    this.setState({ status: e === undefined ? null : e });
  };
  showTime = (date, datestring) => {
    this.setState({ dateUser: date === undefined ? null : datestring });
  };
  addPriority = e => {
    this.setState({ priority: e === undefined ? null : e });
  };
  addTask = () => {
    if (
      this.state.pageData.name !== null &&
      this.state.things !== null &&
      this.state.user !== null &&
      MainProjectName === this.state.pageData.name
    ) {
      TaskArray.push({
        name: this.state.pageData.name,
        things: this.state.things,
        owner: this.state.user,
        status: this.state.status,
        date: this.state.dateUser,
        priority: this.state.priority
      });
    }
    this.props.storeIt(TaskArray);
    this.setState({
      things: null,
      user: null
    });
    document.getElementById("myThings").value = null;
    document.getElementById("myUser").value = null;
    TaskArray.pop();
  };
  onDrop = data => {
    e.preventDefault();
    var data = event.dataTransfer.getData("Text");
    event.target.appendChild(document.getElementById(data));
  };

  render() {
    MainProjectName = this.state.pageData && this.state.pageData.name;

    const columns = [
      {
        title: <b className="thingsToDo">Things to do</b>,
        dataIndex: "things",
        render: text => {
          return <div>{text}</div>;
        }
      },
      {
        title: "Owner",
        dataIndex: "owner",
        render: text => {
          return (
            <div>
              <UserOutlined /> {text}
            </div>
          );
        }
      },
      {
        title: "Status",
        dataIndex: "status",
        render: text => {
          if (text === "Working on it") {
            var yellow = "orange";
            return (
              <div>
                <Tag color={yellow} key={text}>
                  {text.toUpperCase()}
                </Tag>
              </div>
            );
          }
          if (text === "Stuck") {
            var blue = "red";
            return (
              <div>
                <Tag color={blue} key={text}>
                  {text.toUpperCase()}
                </Tag>
              </div>
            );
          }
          if (text === "Working for review") {
            var green = "blue";
            return (
              <div>
                <Tag color={green} key={text}>
                  {text.toUpperCase()}
                </Tag>
              </div>
            );
          }
          if (text === "Done") {
            var green = "green";
            return (
              <div>
                <Tag color={green} key={text}>
                  {text.toUpperCase()}
                </Tag>
              </div>
            );
          }
        }
      },
      {
        title: "Due date",
        dataIndex: "date",
        render: text => {
          return <div>{text}</div>;
        }
      },
      {
        title: "Priority",
        dataIndex: "priority",
        render: text => {
          if (text === "Urgent") {
            var yellow = "black";
            return (
              <div>
                <Tag color={yellow} key={text}>
                  {text.toUpperCase()}
                </Tag>
              </div>
            );
          }
          if (text === "High") {
            var blue = "red";
            return (
              <div>
                <Tag color={blue} key={text}>
                  {text.toUpperCase()}
                </Tag>
              </div>
            );
          }
          if (text === "Medium") {
            var green = "blue";
            return (
              <div>
                <Tag color={green} key={text}>
                  {text.toUpperCase()}
                </Tag>
              </div>
            );
          }
          if (text === "Low") {
            var green = "#98dcdf";
            return (
              <div>
                <Tag color={green} key={text}>
                  {text.toUpperCase()}
                </Tag>
              </div>
            );
          }
        }
      }
    ];
    const dataSource =
      this.props.storeArray !== undefined &&
      this.state.pageData !== undefined &&
      this.props.storeArray.filter((item, index) => {
        return item.name === this.state.pageData.name;
      });

    return (
      <Layout className="layoutContainer">
        <Sider
          style={{
            display: "contents",
            height: "100vh"
          }}
        >
          <img src="colorLogo.png" className="colorLogo" />

          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item key="1" icon={<BellOutlined />}></Menu.Item>
            <Menu.Item key="2" icon={<DownloadOutlined />}></Menu.Item>

            <div className="upgrade"></div>

            <Menu.Item key="3" icon={<FileDoneOutlined />}></Menu.Item>
            <Menu.Item key="4" icon={<UserAddOutlined />}></Menu.Item>
            <Menu.Item key="5" icon={<SearchOutlined />}></Menu.Item>
            <Menu.Item key="6" icon={<QuestionOutlined />}></Menu.Item>

            <img src="userBig.jpg" className="userLogo"></img>
          </Menu>
        </Sider>
        <Sider
          style={{ background: "white" }}
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
        >
          <Menu theme="light" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item style={{ background: "white" }}>
              <h1>Workspaces</h1>
            </Menu.Item>
            <Menu.Item style={{ background: "white" }}>
              <Input.Search placeholder="Search by..." enterButton />
            </Menu.Item>
            <Menu.Item
              className="horizontalContainer"
              style={{ background: "white" }}
            >
              <span className="align">
                <p className="main">
                  <HomeTwoTone />
                </p>
                <h1>Add Project</h1>
              </span>
              <span>
                <FileAddOutlined onClick={this.showModal} />
              </span>
            </Menu.Item>
            {ProjectArray.map((item, index) => {
              return (
                <Menu.Item key={index} onClick={this.pageData} id={index}>
                  {item.name}
                </Menu.Item>
              );
            })}
          </Menu>
        </Sider>
        <Modal
          title="Add Project"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <div class="verticalContainer">
            <span>
              <input
                className="input"
                type="text"
                id="myInput"
                value={this.state.projectName}
                name="name"
                onChange={this.addName}
                placeholder="Enter Project Name"
                required
              />
            </span>
            <span>
              <input
                className="input"
                type="text"
                id="fieldInput"
                value={this.state.projectDescription}
                name="Description"
                onChange={this.addDescription}
                placeholder="Enter Project Description"
                required
              />
            </span>
            <span>
              <input
                className="input"
                type="submit"
                value="submit"
                name="submit"
                onClick={this.addProject}
              />
            </span>
          </div>
        </Modal>
        <Modal
          title="Add Task for the project"
          visible={this.state.show}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <div class="verticalContainer">
            <span>
              <input
                className="input"
                type="text"
                id="myThings"
                value={this.state.things}
                name="things"
                onChange={this.addThings}
                placeholder="Enter Task Name"
                required
              />
            </span>
            <span>
              <input
                className="input"
                type="text"
                id="myUser"
                value={this.state.user}
                name="user"
                onChange={this.addUser}
                placeholder="Enter User Name"
                required
              />
            </span>
            <span>
              <Select
                showSearch
                id="myStatus"
                style={{ width: 200, margin: "10px 10px" }}
                placeholder="Select a Status"
                optionFilterProp="children"
                onChange={this.showStatus}
                onFocus={this.onFocus}
                onBlur={this.onBlur}
                onSearch={this.onSearch}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }
              >
                <Option value="Working on it">Working on it</Option>
                <Option value="Stuck">Stuck</Option>
                <Option value="Working for review">Working for review</Option>
                <Option value="Done">Done</Option>
              </Select>
            </span>
            <span>
              <DatePicker
                onChange={this.showTime}
                style={{ margin: "10px 10px" }}
              />
            </span>
            <span>
              <Select
                showSearch
                id="myPriority"
                style={{ width: 200, margin: "10px 10px" }}
                placeholder="Select a Priority"
                optionFilterProp="children"
                onChange={this.addPriority}
                onFocus={this.onFocus}
                onBlur={this.onBlur}
                onSearch={this.onSearch}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }
              >
                <Option value="Urgent">Urgent</Option>
                <Option value="High">High</Option>
                <Option value="Medium">Medium</Option>
                <Option value="Low">Low</Option>
              </Select>
            </span>
            <span>
              <input
                className="input"
                type="submit"
                value="submit"
                name="submit"
                onClick={this.addTask}
              />
            </span>
          </div>
        </Modal>
        <Layout className="site-layout">
          <Content
            className="site-layout-background"
            style={{
              margin: "0px 5px",
              padding: 24,
              minHeight: 280
            }}
          >
            <div className="headingContainer">
              <span>
                {React.createElement(
                  this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: "trigger",
                    onClick: this.toggle
                  }
                )}
              </span>
              <span className="alignCenter">
                <h1>
                  {this.state.pageData !== undefined &&
                    this.state.pageData.name}
                </h1>
                <p>
                  {this.state.pageData !== undefined &&
                    this.state.pageData.description}
                </p>
              </span>
            </div>
            <div className="toggleTable">
              <span className="toggleTable" onClick={this.toggleTable}>
                <TableOutlined />
                <h1 className="heading">View Task Table</h1>
                {this.state.arrowDown ? (
                  <i className="arrow up"></i>
                ) : (
                  <i className="arrow down"></i>
                )}
              </span>
              <span>
                <Button
                  onClick={this.handleAdd}
                  type="primary"
                  style={{
                    marginBottom: 16,
                    borderRadius: "50px",
                    width: "130px",
                    top: "20px",
                    position: "relative"
                  }}
                >
                  New Item
                  <i className="newArrow down"></i>
                </Button>
              </span>
            </div>

            {this.state.arrowDown && (
              <div className="parent">
                <Draggable>
                  <Table
                    style={{ top: "30px", position: "relative" }}
                    columns={columns}
                    dataSource={dataSource}
                    bordered
                    size="middle"
                  />
                </Draggable>

                <Droppable onDrop={this.onDrop}></Droppable>
              </div>
            )}
          </Content>
        </Layout>
      </Layout>
    );
  }
}
const dispachToProps = dispatch => {
  return {
    storeIt: (...data) => dispatch(storeIt(...data))
  };
};

const stateToProps = state => {
  return {
    storeArray: state.SendReducer.storeArray
  };
};

export default connect(stateToProps, dispachToProps)(Scale);
