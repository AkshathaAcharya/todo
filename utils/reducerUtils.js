/**
* Returns a reducer function
* @param reducerFunctions {object} an object where keys are action types and values are functions
@param initialState {}
*/

function getReducerFromObject(initialState, handlers) {
  return function reducer(state = initialState, action) {
    if (handlers.hasOwnProperty(action.type)) {
      return handlers[action.type](state, action);
    } else {
      return state;
    }
  };
}

export default getReducerFromObject;
