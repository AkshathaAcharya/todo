import axios from "axios";
const axiosCall = (
  url,
  data,
  method = "get",
  headers = {},
  responseType = "json",
  props
) => {
  return new Promise((resolve, reject) => {
    if (headers["headers"]) headers = headers["headers"];
    axios({
      url: `${url}`,
      method,
      data,
      headers: {
        Authorization: localStorage.getItem("auth_token")
      },
      responseType
    })
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        reject(error);
      });
  });
};

export default axiosCall;
